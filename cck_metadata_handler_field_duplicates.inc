<?php

class cck_metadata_handler_field_duplicates extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['filter'] = array('default' => TRUE);
    $options['names'] = array('default' => array());
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['filter'] = array(
      '#title' => t('Use filter'),
      '#description' => t('Check for duplicates based on corresponding filter value. If none found, use the values below.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['filter']
    );

    $form['names'] = array(
      '#title' => t('Metadata'),
      '#description' => t('Metadata entries to check for duplicates.'),
      '#type' => 'select',
      '#options' => cck_metadata_names($this->definition['content_field_name']),
      '#default_value' => $this->options['names'],
      '#multiple' => TRUE,
    );
  }

  function query($group_by = FALSE) {
    // No querying here.
  }

  function pre_render($values) {
    $this->duplicates = array();

    $names = array();
    if (!empty($this->options['filter'])) {
      foreach ($this->view->filter as $filter) {
        if ($filter instanceof cck_metadata_handler_filter_duplicates) {
          $names = (array)$filter->value;
        }
      }
    }
    else {
      $names = (array)$this->options['names'];
    }
    if (empty($names)) return;

    $this->duplicates = cck_metadata_duplicates($this->definition['content_field_name'], $names, TRUE);
  }

  function render($values) {
    return implode(', ', array_map(
      function ($nid) { return l($nid, 'node/' . $nid); },
      (array)$this->duplicates[$values->nid]
    ));
  }
}
