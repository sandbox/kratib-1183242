<?php

class cck_metadata_handler_filter_duplicates extends views_handler_filter_in_operator {
  function get_value_options() {
    $this->value_options = cck_metadata_names($this->definition['content_field_name']);
  }

  function operators() {
    return array(
      'in' => array(
        'title' => t('Has duplicates'),
        'method' => 'op_duplicates',
        'short' => t('has duplicates'),
        'values' => 1,
      ),
      'not in' => array(
        'title' => t('Does not have duplicates'),
        'method' => 'op_duplicates',
        'short' => t('no duplicates'),
        'values' => 1,
      ),
    );
  }

  function op_duplicates() {
    if (empty($this->value)) return;

    $this->ensure_my_table();
    $duplicates = cck_metadata_duplicates($this->definition['content_field_name'], (array)$this->value);
    $duplicates_placeholder = empty($duplicates) ? "''" : db_placeholders($duplicates);
    $this->query->add_where($this->options['group'], "$this->table_alias.nid $this->operator ($duplicates_placeholder)", $duplicates);
  }
}
