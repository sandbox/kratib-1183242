<?php

/**
 * Implements hook_views_data_alter().
 */
function cck_metadata_views_data_alter(&$data) {
  foreach (content_fields() as $field_name => $field) {
    if ($field['type'] == 'metadata') {
      $table = content_views_tablename($field);

      // Turn metadata name filter into a select list.
      $data[$table][$field_name . '_name']['filter']['handler'] = 'cck_metadata_handler_filter_name';

      // Add duplicates field.
      $data[$table][$field_name . '_duplicates'] = array(
        'title' => $field['widget']['label'] . ' (' . $field_name . ') - ' . t("duplicates"),
        'help' => t('Duplicates based on matching metadata'),
        'field' => array(
          'handler' => 'cck_metadata_handler_field_duplicates',
          'content_field_name' => $field_name,
        ),
        'filter' => array(
          'handler' => 'cck_metadata_handler_filter_duplicates',
          'content_field_name' => $field_name,
        ),
      );
    }
  }
}

/**
 * Implements hook_views_handlers().
 */
function cck_metadata_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'cck_metadata'),
    ),
    'handlers' => array(
      'cck_metadata_handler_filter_name' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'cck_metadata_handler_field_duplicates' => array(
        'parent' => 'views_handler_field',
      ),
      'cck_metadata_handler_filter_duplicates' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}
