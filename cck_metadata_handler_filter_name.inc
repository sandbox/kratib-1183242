<?php

class cck_metadata_handler_filter_name extends views_handler_filter_in_operator {
  function get_value_options() {
    $this->value_options = cck_metadata_names($this->definition['content_field_name']);
  }

  function operators() {
    return parent::operators() +
    array(
      'not_exists' => array(
        'title' => t('Does not exist'),
        'method' => 'op_not_exists',
        'short' => t('does not exist'),
        'values' => 1,
      ),
    );
  }

  function op_not_exists() {
    if (empty($this->value)) return;

    $table = $this->definition['table'];
    $field = $this->definition['field'];
    $placeholder = db_placeholders($this->value, 'varchar');
    $subquery = "
SELECT *
FROM {node} nn
LEFT JOIN {{$table}} cc ON nn.vid = cc.vid
WHERE cc.$field IN ($placeholder)
AND nn.nid = node.nid
    ";

    $this->query->add_where($this->options['group'], "NOT EXISTS ($subquery)", (array)$this->value);
  }
}
