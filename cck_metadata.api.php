<?php 

/**
 * @defgroup cck_metadata_hooks CCK Metadata hook functions
 *
 * Core hooks for the CCK metadata module.
 */

/**
 * @file
 * API documentation file.
 *
 * @ingroup cck_metadata_hooks
 */

/**
 * Override field properties per metadata name.
 *
 * @return
 *   An array indexed by metadata name. Each entry is an array containing one or more of the 
 *   following keys:
 *   - formatter - the field formatter callback used to render the given metadata value
 *   - form - the Drupal Form API structure used to input the given metadata value
 */
function hook_cck_metadata_fields() {
  return array(
    'isbn:thumbnail' => array(
      'formatter' => 'mediatheque_formatter_thumbnail',
    ),
    'isbn:description' => array(
      'form' => array(
        '#type' => 'textarea',
      ),
    ),
  );
}

/**
 * Alter the complete field properties array.
 *
 * @param &$fields
 *   The array containing all entries as returned by hook_cck_metadata_fields() above.
 */
function hook_cck_metadata_fields_alter(&$fields) {
	$fields['isbn:description']['form']['#description'] = t('Some help text here.');
}
